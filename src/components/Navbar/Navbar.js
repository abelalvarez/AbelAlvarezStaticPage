import React, { Component } from 'react';
import { Navbar, NavbarBrand, NavbarNav, NavbarToggler, Collapse, NavItem, NavLink } from 'mdbreact';
import { BrowserRouter as Router } from 'react-router-dom';

const ABOUT = 'About';
const PROJECTS = 'Projects';
const DOWNLOADS = 'Downloads';
const CONTACT = 'Contact';

class NavbarFeatured extends Component {
    constructor(props) {
        super(props);

        this.onClick = this.onClick.bind(this);
        this.toggle = this.toggle.bind(this);
        const location = window.location.hash.replace('#', '');

        this.state = {
            collapse: false,
            isWideEnough: false,
            dropdownOpen: false,
            currentTab: this.getCurrentTab(location.toLowerCase())
        };
    }

    getCurrentTab(location) {
        switch (location) {
            case ABOUT.toLowerCase():
                return ABOUT;
            case PROJECTS.toLowerCase():
                return PROJECTS;
            case DOWNLOADS.toLowerCase():
                return DOWNLOADS;
            case CONTACT.toLowerCase():
                return CONTACT;
            default:
                return ABOUT;
        }
    }

    onClick() {
        this.setState({
            collapse: !this.state.collapse,
        });
    }

    toggle() {
        this.setState({
            dropdownOpen: !this.state.dropdownOpen
        });
    }

    onItemChange = (item) => {
        this.setState({ currentTab: item });
    }

    isActive(selected) {
        return (this.state.currentTab === selected) ? 'active' : null;
    }

    render() {
        return (
            <Router>
                <Navbar color="indigo" dark expand="md" scrolling>
                    <NavbarBrand href="#">
                        <strong>Abel Alvarez</strong>
                    </NavbarBrand>
                    {!this.state.isWideEnough && <NavbarToggler onClick={this.onClick} />}
                    <Collapse isOpen={this.state.collapse} navbar>
                        <NavbarNav right>
                            <NavItem className={this.isActive(ABOUT)} >
                                <NavLink to={`#${ABOUT.toLowerCase()}`} onClick={() => this.onItemChange(ABOUT)}>{ABOUT}</NavLink>
                            </NavItem>
                            <NavItem className={this.isActive(PROJECTS)} >
                                <NavLink to={`#${PROJECTS.toLowerCase()}`} onClick={() => this.onItemChange(PROJECTS)}>{PROJECTS}</NavLink>
                            </NavItem>
                            <NavItem className={this.isActive(DOWNLOADS)} >
                                <NavLink to={`#${DOWNLOADS.toLowerCase()}`} onClick={() => this.onItemChange(DOWNLOADS)}>{DOWNLOADS}</NavLink>
                            </NavItem>
                            <NavItem className={this.isActive(CONTACT)} >
                                <NavLink to={`#${CONTACT.toLowerCase()}`} onClick={() => this.onItemChange(CONTACT)}>{CONTACT}</NavLink>
                            </NavItem>
                        </NavbarNav>
                    </Collapse>
                </Navbar>
            </Router>
        );
    }
}

export default NavbarFeatured;