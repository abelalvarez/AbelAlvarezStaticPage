import React, { Component } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import 'mdbreact/dist/css/mdb.css';
import './App.css';
import NavbarFeatured from './components/Navbar/Navbar';

class App extends Component {
  render() {
    return (
      <div className="App">
        <header >
          <NavbarFeatured/>
        </header>
        <p className="App-intro">
          To get started call me, edit <code>src/App.js</code> and save to reload.
          <button className="btn btn-primary">test</button>
        </p>
      </div>
    );
  }
}

export default App;
